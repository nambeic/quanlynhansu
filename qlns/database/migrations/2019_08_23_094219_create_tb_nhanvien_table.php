<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbNhanvienTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_nhanvien', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hoTen');
            $table->string('diaChi');
            $table->bigInteger('id_phong')->unsigned(); 
            $table->foreign('id_phong')->references('id')->on('tb_phong')->onDelete('cascade');
            $table->bigInteger('sdt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_nhanvien');
    }
}
