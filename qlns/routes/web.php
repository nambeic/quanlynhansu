<?php

Route::get('/', function () {
    return view('login');
});

Route::resource('phong', 'PhongController')->middleware('admin');
Route::resource('nhanvien', 'NhanVienController');

Route::get('login', 'LoginController@getLogin')->name('login');
Route::post('login', 'LoginController@postLogin');
Route::get('logout', 'LogoutController@getLogout');
