$(function() {
    $('#dang-nhap').click(function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            'url': 'login',
            'data': {
                'email': $('#email').val(),
                'password': $('#password').val()
            },
            'type': 'POST',
            success: function(data) {
                if (data.error == true) {
                    $('.error').hide();

                    if (data.message.errorlogin != undefined) {
                        $('.errorLogin').show().text(data.message.errorlogin[0]);
                    }
                } else {
                    window.location.href = "nhanvien"
                }
            },
            error: function(data) {
                var err = data.responseJSON.errors;
                if (err.email[0] != undefined) {
                    $('.errorEmail').show().text(err.email[0]);
                }
                if (err.password[0] != undefined) {
                    $('.errorPassword').show().text(err.password[0]);
                }
            }
        });
    })
});