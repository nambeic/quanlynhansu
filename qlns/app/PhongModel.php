<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PhongModel extends Model
{
    protected $table = 'tb_phong';
    protected $fillable = [
        'ten_phong', 'mo_ta',
    ];
    public function quanly()
    {
        return $this->belongsTo('App\NhanVienModel', 'id_phong', 'id');
    }
    public static function index()
    {
        if (request()->ajax()) {
            return datatables()->of(PhongModel::latest()->get())
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="edit" id="' . $data->id . '" class="edit btn btn-primary btn-sm">Sửa</button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm">Xóa</button>';
                    $button .= '&nbsp;&nbsp;';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('phong');
    }
    public static function store($request)
    {
        $form_data = array(
            'ten_phong' => $request->ten_phong,
            'mo_ta' => $request->mo_ta,
        );
        PhongModel::create($form_data);
    }

    public static function destroy($id)
    {
        $data = PhongModel::findOrFail($id);
        $data->delete();
    }

    public static function edit($id)
    {
        if (request()->ajax()) {
            $data = PhongModel::findOrFail($id);
            return $data;
        }
    }

    public static function update1($request)
    {
        $form_data = array(
            'ten_phong' => $request->ten_phong,
            'mo_ta' => $request->mo_ta,
        );
        PhongModel::whereId($request->hidden_id)->update($form_data);
    }
}
