<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Password implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function passes($attribute, $value)
    {
        return $value != "" && strlen($value) >= 8;
    }

    public function message()
    {
        return 'Mật khẩu không đúng định dạng.';
    }
}
