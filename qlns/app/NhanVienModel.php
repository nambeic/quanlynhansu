<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class NhanVienModel extends Model
{
    protected $table = 'tb_nhanvien';
    protected $fillable = [
        'hoTen', 'diaChi', 'id_phong', 'sdt',
    ];
    public function phong()
    {
        return $this->belongsTo('App\PhongModel', 'id_phong', 'id');
    }
    public static function index()
    {
        if (request()->ajax()) {
            return datatables()->of(NhanVienModel::latest()->get())
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="edit" id="' . $data->id . '" class="edit btn btn-primary btn-sm">Sửa</button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm">Xóa</button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('nhanvien');
    }
    public static function store($request)
    {
        $form_data = array(
            'hoTen' => $request->hoTen,
            'diaChi' => $request->diaChi,
            'id_phong' => $request->id_phong,
            'sdt' => $request->sdt,

        );
        NhanVienModel::create($form_data);
    }

    public static function edit($id)
    {
        if (request()->ajax()) {
            $data = NhanVienModel::findOrFail($id);
            return $data;
        }
    }

    public static function update1($request)
    {
        $form_data = array(
            'hoTen' => $request->hoTen,
            'diaChi' => $request->diaChi,
            'id_phong' => $request->id_phong,
            'sdt' => $request->sdt,
        );
        NhanVienModel::whereId($request->hidden_id)->update($form_data);
    }

    public static function destroy($id)
    {
        $data = NhanVienModel::findOrFail($id);
        $data->delete();
    }
    public static function show_id($id)
    {
        switch ($id) {
            case 1:
                if (request()->ajax()) {
                    return datatables()->of(NhanVienModel::latest()->where('id_phong', 1)->get())
                        ->addColumn('action', function ($data) {
                            $button = '<button type="button" name="edit" id="' . $data->id . '" class="edit btn btn-primary btn-sm">Sửa</button>';
                            $button .= '&nbsp;&nbsp;';
                            $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm">Xóa</button>';
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                }
                return view('DanhSach');
                break;
            case 2:
                if (request()->ajax()) {
                    return datatables()->of(NhanVienModel::latest()->where('id_phong', 2)->get())
                        ->addColumn('action', function ($data) {
                            $button = '<button type="button" name="edit" id="' . $data->id . '" class="edit btn btn-primary btn-sm">Sửa</button>';
                            $button .= '&nbsp;&nbsp;';
                            $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm">Xóa</button>';
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                }
                return view('DanhSach');
                break;
            case 3:
                if (request()->ajax()) {
                    return datatables()->of(NhanVienModel::latest()->where('id_phong', 3)->get())
                        ->addColumn('action', function ($data) {
                            $button = '<button type="button" name="edit" id="' . $data->id . '" class="edit btn btn-primary btn-sm">Sửa</button>';
                            $button .= '&nbsp;&nbsp;';
                            $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm">Xóa</button>';
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                }
                return view('DanhSach');
                break;
            case 4:
                if (request()->ajax()) {
                    return datatables()->of(NhanVienModel::latest()->where('id_phong', 4)->get())
                        ->addColumn('action', function ($data) {
                            $button = '<button type="button" name="edit" id="' . $data->id . '" class="edit btn btn-primary btn-sm">Sửa</button>';
                            $button .= '&nbsp;&nbsp;';
                            $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm">Xóa</button>';
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                }
                return view('DanhSach');
                break;
        }
    }
}
