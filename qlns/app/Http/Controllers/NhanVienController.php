<?php

namespace App\Http\Controllers;

use App\Http\Requests\InputNhanVienRequest;
use App\NhanVienModel;

class NhanVienController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);
    }
    public function index()
    {
        return NhanVienModel::index();
    }
    public function show($id)
    {
        return NhanVienModel::show_id($id);
    }
    public function store(InputNhanVienRequest $request)
    {
        NhanVienModel::store($request);
        return response()->json(['success' => 'Thêm thành công.']);
    }
    public function edit($id)
    {
        $data = NhanVienModel::edit($id);
        return response()->json(['data' => $data]);
    }
    public function update(InputNhanVienRequest $request)
    {
        NhanVienModel::update1($request);
        return response()->json(['success' => 'Chỉnh sửa thành công']);
    }
    public function destroy($id)
    {
        return NhanVienModel::destroy($id);
    }
}
