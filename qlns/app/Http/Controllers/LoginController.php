<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\User;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('login');
    }
    public function postLogin(LoginRequest $request)
    {
        return User::postLogin($request);
    }
}
