<?php

namespace App\Http\Controllers;

use App\Http\Requests\InputPhongRequest;
use App\PhongModel;

class PhongController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);
    }

    public function index()
    {
        return PhongModel::index();
    }
    public function store(InputPhongRequest $request)
    {
        PhongModel::store($request);
        return response()->json(['success' => 'Thêm thành công.']);
    }
    public function show($id)
    {

    }
    public function edit($id)
    {
        $data = PhongModel::edit($id);
        return response()->json(['data' => $data]);
    }
    public function update(InputPhongRequest $request, $id)
    {
        PhongModel::update1($request);
        return response()->json(['success' => 'Chỉnh sửa thành công']);
    }
    public function destroy($id)
    {
        return PhongModel::destroy($id);
    }
}
