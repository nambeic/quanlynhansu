<?php

namespace App\Http\Requests;

use App\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => [new Password],
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'Không được bỏ trống trường Email',
            'email.email' => 'Định dang email không đúng',
        ];
    }
}
