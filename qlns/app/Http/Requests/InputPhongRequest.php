<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InputPhongRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ten_phong' => 'required',
            'mo_ta' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'ten_phong.required' => 'Tên phòng không được bỏ trống.',
            'mo_ta.required' => 'Mô tả không được bỏ trống.',

        ];
    }
}
