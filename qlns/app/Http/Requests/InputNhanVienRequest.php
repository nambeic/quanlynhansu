<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InputNhanVienRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'hoTen' => 'required',
            'diaChi' => 'required',
            'id_phong' => 'required|numeric',
            'sdt' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'hoTen.required' => 'Họ không được bỏ trống.',
            'diaChi.required' => 'Tên không được bỏ trống.',
            'id_phong.required' => 'ID phòng không được bỏ trống.',
            'sdt.required' => 'Số điện thoại không được bỏ trống.',
            'sdt.numeric' => 'Số điện thoại phải là số.',
            'id_phong.numeric' => 'ID phòng phải là số.',
        ];
    }
}
