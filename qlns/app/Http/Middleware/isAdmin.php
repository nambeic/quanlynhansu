<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class isAdmin
{

    public function handle($request, Closure $next)
    {
        if (auth()->user()->isAdmin() == 1) {
            return $next($request);
        }
        return redirect('nhanvien')->with('status', 'Bạn phải là Admin mới có quyền truy cập');
    }
}
