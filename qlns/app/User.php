<?php

namespace App;

use App\Http\Requests\LoginRequest;
use Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\MessageBag;

class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password', 'type',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function postLogin(LoginRequest $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password], $request->has('remember'))) {
            return response()->json([
                'error' => false,
                'message' => 'success',
            ], 200);
        } else {
            $errors = new MessageBag(['errorlogin' => 'Email hoặc mật khẩu không chính xác']);
            return response()->json([
                'error' => true,
                'message' => $errors,
            ], 200);
        }
    }

    public function isAdmin()
    {
        return $this->type == 1;
    }
}
